import xgboost as xgb
import pandas as pd
import cleaner
import os
import joblib
import trainer


# gets the dictionary list from file Cf cleaner.saveListToFile
def getListFromFile(name):
    file = open(name, 'r')
    lines = file.readlines()
    l = [line.rstrip() for line in lines]
    return l


# converts list of strings (vallist) to list of integers following dictlist the reverse operation of cleaner.inconvert_
def valueToInt(dictlist, vallist):
    l = []
    for value in vallist:
        if value in dictlist:
            l.append(dictlist.index(value) + 1)
        else:
            l.append(0)
    return l


# converts arguments to a pandas.DataFrame following the model
def valuesToTestable(actors, studio, language, genres, producers, budget, month, year, length):
    typelist = getListFromFile('data/type1')
    actorlist = getListFromFile('data/actor1')
    studiolist = getListFromFile('data/prod1')
    languagelist = getListFromFile('data/original_language')
    producerlist = getListFromFile('data/ExecutiveProducer')
    intactors = valueToInt(actorlist, actors)
    intgenres = valueToInt(typelist, genres)
    intstudio = valueToInt(studiolist, studio)
    intlanguage = valueToInt(languagelist, language)
    intproducers = valueToInt(producerlist, producers)
    d = {'budget': [budget], 'original_language': [intlanguage[0]], 'runtime': [length], 'Director': [intproducers[0]],
         'Producer': [intproducers[1]], 'ExecutiveProducer': [intproducers[2]], 'CoProducer': [intproducers[3]],
         'actor1': [intactors[0]], 'actor2': [intactors[1]], 'actor3': [intactors[2]], 'actor4': [intactors[3]],
         'prod1': [intstudio[0]], 'type1': [intgenres[0]], 'type2': [intgenres[1]], 'month': [month], 'year': [year]}
    return pd.DataFrame(d)


# cleans incoherent budgets from test data
def cleanBudget(string):
    string2 = string
    for i in string.index:
        if string.budget[i] < 50000:
            string2 = string2.drop(i)
    return string2


# generates a random test if it doesn't exist, beware the benefits.txt only works for the current CleanTest
# any new test generations may falsify the end count
def getCleanTest():
    if not os.path.exists("data/CleanTest.csv"):
        testData = pd.read_csv("data/test.csv")

        test2 = testData.filter(
            items=['budget', 'genres', 'original_language', 'production_companies', 'release_date', 'runtime', 'cast',
                   'crew', 'title'])
        test2 = test2.dropna()
        test2 = cleanBudget(test2)
        test2 = test2.sample(n=50)
        cleaner.director(test2, "crew", "Director", "Director", 1)
        cleaner.director(test2, "crew", "Producer", "Producer", 1)
        cleaner.director(test2, "crew", "Executive Producer", "ExecutiveProducer", 1)
        cleaner.director(test2, "crew", "Co-Producer", "CoProducer", 1)
        cleaner.textToData(test2, 4, "name", "cast", "actor")
        cleaner.textToData(test2, 1, "name", "production_companies", "prod")
        cleaner.textToData(test2, 2, "name", "genres", "type")
        cleaner.date(test2, "release_date")
        test2.to_csv("data/CleanTest.csv")
        return test2
    else:
        return pd.read_csv("data/CleanTest.csv")


# dialog for training functions
def trainDialog():
    while True:
        text = input("Would you like me to train xgb or random forest?(xgb or rf)")
        if text == "xgb":
            trainer.train()
            return
        elif text == "rf":
            trainer.train(1)
            return


# creates a label based on ratio benefits/budget, same calculations as for the training label
def budgetToRatio(movies):
    benefits = getListFromFile("data/benefits.txt")
    l = []
    for i in range(0, len(movies.budget)):
        label = 0
        ratio = int(benefits[i]) / movies.budget[i]
        if ratio < 1:
            label = 1
        elif ratio <= 1.5:
            label = 2
        elif ratio <= 2:
            label = 3
        elif ratio <= 3:
            label = 4
        elif ratio <= 4:
            label = 5
        elif ratio <= 5:
            label = 6
        elif ratio <= 6:
            label = 7
        elif ratio > 6:
            label = 8
        else:
            print("bug, ratio impossible")
        l.append(label)
    return l


# cleaning
while True:
    text = input("Would you like me to clean again?(yes or no)")
    if text == "yes":
        cleaner.createCleanFile()
        break
    elif text == "no":
        break

# training
while True:
    text = input("Would you like me to re train ?(yes or no)")
    if text == "yes":
        trainDialog()
        break
    elif text == "no":
        break

# algo choice
while True:
    text = input("Would you like me to use xgb or random Forest?(xgb or rf)")
    if text == "xgb":
        clf = xgb.XGBClassifier(n_estimators=100000, max_depth=1000, n_jobs=-1)
        clf.load_model('models/xgbgradiendboost.xgb')
        break
    elif text == "rf":
        clf = joblib.load('models/randomforest.joblib')
        break

movies = getCleanTest()
trueratio = budgetToRatio(movies)
o = 0
m = 0
for i in range(0, len(movies)):
    r = valuesToTestable([movies["actor1"][i], movies["actor2"][i], movies["actor3"][i], movies["actor4"][i]],
                         [movies["prod1"][i]], movies.original_language[i], [movies["type1"][i], movies["type2"][i]],
                         [movies.Director[i], movies.Producer[i], movies.ExecutiveProducer[i], movies.CoProducer[i]],
                         movies.budget[i], movies.month[i], movies.year[i], movies.runtime[i])
    ratio = clf.predict(r)
    print(movies.title[i] + ": " + "true:" + str(trueratio[i]) + " prediction:" + str(ratio))
    if ratio == trueratio[i] or ratio == trueratio[i] + 1 or ratio == trueratio[i] - 1:
        o += 1
    if ratio < trueratio[i]:
        m += 1
print("Number of exact predictions: " + str(o))
print("Number of sub evaluations: " + str(m))

print("Spider man: No way home " + str(clf.predict(
    valuesToTestable(['Benedict Cumberbatch', 'Tom Holland', 'Zendaya', 'Marisa Tomei'], ['Marvel Studios'],
                     'en', ['Action', 'Adventure'],
                     ['Jon Watts', 'Kevin Feige', 'JoAnn Perritano', 'Mitchell Bell'], 180000000, 12, 21,
                     150))))

print("Matrix Resurrections " + str(clf.predict(
    valuesToTestable(['Keanu Reeves', 'Laurence Fishburne', 'Carrie-Anne Moss', 'Carrie-Anne Moss'], ['Warner Bros.'],
                     'en', ['Adventure', 'Science Fiction'],
                     ['Lana Wachowski', 'Grant Hill ', 'José Luis Escolar', 'Miki Emmrich'], 1000000000, 12, 21,
                     148))))
