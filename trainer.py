import pandas as pd
import joblib
from sklearn.ensemble import RandomForestClassifier
import xgboost as xgb


# trains a model: XGB gradient boosting classifier, by default, randomForest otherwise
def train(algo=0):
    train = pd.read_csv("data/clean.csv")

    ratioclass = []
    for i in train.index:
        label = 0
        ratio = train.ratio[i]
        if ratio < 1:
            label = 1
        elif ratio <= 1.5:
            label = 2
        elif ratio <= 2:
            label = 3
        elif ratio <= 3:
            label = 4
        elif ratio <= 4:
            label = 5
        elif ratio <= 5:
            label = 6
        elif ratio <= 6:
            label = 7
        elif ratio > 6:
            label = 8
        else:
            print("bug, ratio impossible")
        ratioclass.append(label)
    x = train.filter(items=["budget", "original_language", "runtime", "Director",
                            "Producer", "ExecutiveProducer", "CoProducer",
                            "actor1", "actor2", "actor3", "actor4",
                            "prod1", "type1", "type2", "month", "year"])

    y = ratioclass
    if algo == 0:
        clf = xgb.XGBClassifier(n_estimators=100000, max_depth=1000, n_jobs=-1)
        decisiontree = clf.fit(X=x, y=y)
        decisiontree.save_model('models/xgbgradiendboost.xgb')

    else:
        clf = RandomForestClassifier(n_estimators=20000, max_depth=1000, n_jobs=-1)
        decisiontree = clf.fit(X=x, y=y)
        joblib.dump(decisiontree, "models/randomforest.joblib")
