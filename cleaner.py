import pandas as pd
import ast
import json


# creates the useful data of ratio : revenue/budget and cleans up some data inconsistencies
def ratio_crea(dframe):
    ratio = []
    t = dframe
    l = []
    for i in dframe.index:
        if dframe.revenue[i] < 50000 or dframe.budget[i] < 50000:
            l.append(i)
        else:
            ratio.append(dframe.revenue[i] / dframe.budget[i])
    t = t.drop(l)
    t['ratio'] = ratio
    return t


# transforms json formatted data for crew into appropriately named columns
def director(dframe, ncol, job, title, nbr):
    for a in range(0, nbr):
        l = []
        for i in dframe.index:
            lim = 0
            res = ast.literal_eval(dframe[ncol][i])
            for j in range(0, len(res)):
                s2 = json.dumps(res[j])
                s3 = json.loads(s2)
                if s3["job"] == job and lim < 1:
                    l.append(s3["name"])
                    lim += 1
            if lim == 0:
                l.append(None)
        dframe[title] = l


# transforms json formatted data for cast, genres, productions and language into appropriately named columns
def textToData(dframe, nbr, name, ncol, title):
    for i in range(0, nbr):
        l = []
        for j in dframe.index:
            res = ast.literal_eval(dframe[ncol][j])
            if i < len(res):
                s2 = json.dumps(res[i])
                s3 = json.loads(s2)
                l.append(s3[name])
            else:
                # print("null")
                l.append(None)
        dframe[title + str(i + 1)] = l


# injective converter function from string to integer using a single column
def intconvert_one(dframe, ncol1):
    t = dframe
    p = dframe[ncol1]
    mylist = list(dict.fromkeys(p))
    saveListToFile(mylist, ncol1)
    i = 0
    while i < len(mylist):
        if type(mylist[i]) is str:
            t = t.replace(mylist[i], i + 1)
        i += 1
    return t


# injective converter function from string to integer using 2 columns
def intconvert_two(dframe, ncol1, ncol2):
    t = dframe
    l = dframe[ncol1]
    l2 = dframe[ncol2]
    p = l.append(l2)
    mylist = list(dict.fromkeys(p))
    # print(mylist)
    saveListToFile(mylist, ncol1)
    i = 0
    while i < len(mylist):
        if type(mylist[i]) is str:
            t = t.replace(mylist[i], i + 1)
        i += 1
    return t


# injective converter function from string to integer using 4 columns
def intconvert_four(dframe, ncol1, ncol2, ncol3, ncol4):
    t = dframe
    l = dframe[ncol1]
    l2 = dframe[ncol2]
    l3 = dframe[ncol3]
    l4 = dframe[ncol4]
    t = t.drop([ncol1, ncol2, ncol3, ncol4], axis=1)
    p = l.append(l2).append(l3).append(l4)
    mylist = list(dict.fromkeys(p))
    saveListToFile(mylist, ncol1)
    i = 0
    while i < len(mylist):
        if type(mylist[i]) is str:
            l = l.replace(mylist[i], i + 1)
            l2 = l2.replace(mylist[i], i + 1)
            l3 = l3.replace(mylist[i], i + 1)
            l4 = l4.replace(mylist[i], i + 1)
        i += 1
    t[ncol1] = l
    t[ncol2] = l2
    t[ncol3] = l3
    t[ncol4] = l4
    return t


# saves the functions to a file : the order of strings in the list is the function
def saveListToFile(list, name):
    file = open('data/' + name, mode='w')
    for i in range(0, len(list)):
        file.write(str(list[i]))
        file.write('\n')
    file.close()


# converts date from string "mm/dd/yy" to two columns: month and year
def date(dframe, nbcol):
    year = []
    month = []
    for i in dframe.index:
        l = dframe[nbcol][i].split('/')
        year.append(int(l[2]))
        month.append(int(l[0]))
    dframe['month'] = month
    dframe['year'] = year


# the aggregate function
def createCleanFile():
    # get train data
    train = pd.read_csv("data/train.csv")
    # drop useless data
    train2 = train.filter(
        items=['budget', 'genres', 'original_language', 'production_companies', 'release_date', 'runtime', 'cast',
               'crew',
               'revenue'])
    # format the date
    date(train2, "release_date")
    # get ratio and clean nulls
    train2 = train2.dropna()
    train2 = ratio_crea(train2)
    # get most recognized crew members
    director(train2, "crew", "Director", "Director", 1)
    director(train2, "crew", "Producer", "Producer", 1)
    director(train2, "crew", "Executive Producer", "ExecutiveProducer", 1)
    director(train2, "crew", "Co-Producer", "CoProducer", 1)
    # get 4 leading actors
    textToData(train2, 4, "name", "cast", "actor")
    # get the production company
    textToData(train2, 1, "name", "production_companies", "prod")
    # get 2 main genres
    textToData(train2, 2, "name", "genres", "type")
    # convert genres to integers, same genre =>same integer
    train2 = intconvert_two(train2, "type1", "type2")
    # convert actors to integers, same name =>same integer
    train2 = intconvert_four(train2, "actor1", "actor2", "actor3", "actor4")
    # convert actors to integers, same production company =>same integer
    train2 = intconvert_one(train2, "prod1")
    # convert actors to integers, same name =>same integer
    train2 = intconvert_four(train2, "ExecutiveProducer", "Director", "Producer", "CoProducer")
    # convert actors to integers, same same language =>same integer
    train2 = intconvert_one(train2, "original_language")
    # replaces null values with 0 for conversions, for example with 1genre movies, or 3 actor movies
    train2.fillna(0, inplace=True)
    # saves everything to a file named clean.csv
    train2.to_csv('data/clean.csv')
